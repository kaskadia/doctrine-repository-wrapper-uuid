<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperUuid;

use Illuminate\Support\ServiceProvider;
use Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Repositories\DoctrineWrapperRepository;
use Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Repositories\Interfaces\IDoctrineWrapperRepository;

class DoctrineRepositoryWrapperProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->bind(IDoctrineWrapperRepository::class, DoctrineWrapperRepository::class);
	}
}