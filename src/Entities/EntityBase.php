<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Entities;

use Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Entities\Interfaces\IEntity;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\ORM\Mapping\{Id,Column,CustomIdGenerator};

abstract class EntityBase implements IEntity {
	/**
	 * EntityBase constructor.
	 * @throws Exception
	 */
	public function __construct() {
		if(!isset($this->id)) {
			$this->id = Uuid::uuid7()->toString();
		}
	}

	/**
         * @Id
	 * @Column(type="uuid", unique=true)
	 * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
	 */
	#[Id]
	#[Column(type: "uuid", unique: true)]
	#[CustomIdGenerator(class: UuidGenerator::class)]
	protected string $id;

	public function getId(): string {
		return $this->id;
	}

	/**
	 * Only sets id if unset. This is primarily for deserialization.
	 * We still want this property to be immutable.
	 * @param string $id
	 */
	public function setId(string $id): self {
		if(!isset($this->id)) {
			$this->id = $id;
		}
		return $this;
	}
}
