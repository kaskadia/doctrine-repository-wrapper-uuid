<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Entities\Interfaces;

interface IEntity {
	public function getId(): string;
	public function setId(string $id): self;
}
