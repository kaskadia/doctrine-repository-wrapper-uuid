<?php /** @noinspection PhpUndefinedFieldInspection */

namespace Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Traits\Repositories;

use Doctrine\Common\Collections\Collection;
use Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Entities\Interfaces\IEntity;
use Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Repositories\Interfaces\IDoctrineWrapperRepository;

trait BaseRepository {
	public function findAll(): Collection {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		return $repo->findAll();
	}

	public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): Collection {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		return $repo->findBy($criteria, $orderBy, $limit, $offset);
	}

	public function save(IEntity $entity): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->save($entity);
	}

	public function flush(): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->flush();
	}

	public function saveAndFlush(IEntity $entity): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->saveAndFlush($entity);
	}

	public function delete(IEntity $entity): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->getEntityManager()->remove($entity);
	}

	public function deleteAndFlush(IEntity $entity): void {
		$this->delete($entity);
		$this->flush();
	}
}
