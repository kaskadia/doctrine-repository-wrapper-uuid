<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperUuid\Traits\Entities;

use Doctrine\ORM\Mapping\Column;

trait Types {
	/**
   * @Column(type="string")
	 */
	#[Column]
	protected string $name;

	/**
   * @Column(type="string")
	 */
	#[Column]
	protected string $slug;

	public function getName(): string {
		return $this->name;
	}

	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}

	public function getSlug(): string {
		return $this->slug;
	}

	public function setSlug(string $slug): self {
		$this->slug = $slug;
		return $this;
	}
}
